#jupiter

```
1.安装virtualenv
	sudo pip install virtualenv
2.新建隔离环境，安装django，新建项目
	virtualenv ENV
	. ./ENV/bin/activate
	pip install django
	django-admin startproject jupiter
3.冒烟测试
	cd jupiter
	python manage.py runserver
	然后访问http://127.0.0.1:8000/和http://127.0.0.1:8000/admin/ 可以看到主页面和后台管理页面
	生成表，创建管理员
	python manage.py migrate
	python manage.py createsuperuser 根据提示输入用户名和密码
	然后再次登录http://127.0.0.1:8000/admin/
4.修改时区和语言jupiter/setting.py
	LANGUAGE_CODE = 'zh-hans' 	TIME_ZONE = 'Asia/Shanghai'
5.添加新app
	python manage.py startapp api
6.运行所有单元测试
    python manage.py test
```