#!/bin/bash
uwsgixml="/home/[user]/workspace/jupiter/uwsgi.xml"
touchfile="/home/[user]/workspace/jupiter/bin/touch_reload_uwsgi"
touchlogfile="/home/[user]/workspace/jupiter/bin/touch_reopen_log"

/usr/bin/uwsgi --uid [user] -x ${uwsgixml} --touch-reload=${touchfile} --touch-logreopen=${touchlogfile}
