# -*- coding:utf-8 -*-
from django.contrib.admin import AdminSite
from django.contrib.auth.models import User, Group
from django.contrib.auth.admin import UserAdmin, GroupAdmin


class MafiaAdminSite(AdminSite):
    site_header = u'木星'
    site_title = u'木星'
    index_title = u'大陆'

jupiter_site = MafiaAdminSite(name='jadmin')

jupiter_site.register(User, UserAdmin)
jupiter_site.register(Group, GroupAdmin)
