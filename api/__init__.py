# -*- coding:utf-8 -*-
"""
run test case
python manage.py test api
"""
from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class ApiConfig(AppConfig):
    name = 'api'
    verbose_name = _(u'API(alpha)')

default_app_config = 'api.ApiConfig'
