# -*- coding:utf-8 -*-
from django.db import models


class GP(models.Model):
    class Meta:
        verbose_name = verbose_name_plural = u'全剧属性'

    name = models.CharField('名称', max_length=64)
    value = models.CharField('取值', max_length=255)

    created_time = models.DateTimeField('创建时间', auto_now_add=True)
    last_modified = models.DateTimeField('最近更新', auto_now=True)

    def __unicode__(self):
        return self.name
