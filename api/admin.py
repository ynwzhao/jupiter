# -*- coding:utf-8 -*-
from django.contrib import admin

from .models import GP
from jupiter.jupiter_admin import jupiter_site


class GPAdmin(admin.ModelAdmin):
    list_display = ['name', 'value', 'created_time', 'last_modified']
    search_fields = ['name', 'value']

jupiter_site.register(GP, GPAdmin)
