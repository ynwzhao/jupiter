# -*- coding:utf-8 -*-
from django.http import JsonResponse


def index_view(request):
    """
    method: GET
    api: /
    api: /api/
    """
    return JsonResponse({
        'host': request.get_host(),
        'api': request.path,
    })
