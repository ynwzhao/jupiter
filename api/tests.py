# -*- coding:utf-8 -*-
"""
python manage.py test api.tests
"""
import json
from django.test import TestCase, Client


class DemoTest(TestCase):
    """
    python manage.py test api.tests.DemoTest
    """
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_add_nums(self):
        """
        python manage.py test api.tests.DemoTest.test_add_nums
        """
        self.assertEquals(2 + 3, 5)

    def test_add_strings(self):
        """
        python manage.py test api.tests.DemoTest.test_add_strings
        """
        self.assertEquals('Hello' + ' ' + 'world !', 'Hello world !')

    def test_home_page(self):
        """
        python manage.py test api.tests.DemoTest.test_home_page
        """
        path = '/'

        c = Client()
        response = c.get(path)

        self.assertEquals(response.status_code, 200)
        json_content = json.loads(response.content)
        self.assertEquals(json_content['api'], path)
